package com.demo.dictionary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DictionaryMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(DictionaryMvcApplication.class, args);
	}

}
